//
//  TGAPNAction.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

class TGAPNAction: Codable {
    let action: TGCustomAction
    let targetId: String?
    let entityType: TGEntityType
    let entityId: Int?
    let entityName: String?
    
    // TODO: Handling of APNs is not a part of Sample Project
//    let customData: CustomData = [String: Any]
    
    enum CodingKeys: String, CodingKey {
        case action
        case targetId
        case entityType
        case entityId
        case entityName
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if let actionString = try values.decodeIfPresent(String.self, forKey: .action), let action = Int(actionString) {
            self.action = TGCustomAction(rawValue: action) ?? .undefined
        } else {
            self.action = .undefined
        }
        
        self.targetId = try values.decodeIfPresent(String.self, forKey: .targetId)
        
        if let entityTypeString = try values.decodeIfPresent(String.self, forKey: .entityType), let entityType = Int(entityTypeString) {
            self.entityType = TGEntityType(rawValue: entityType) ?? .undefined
        } else {
            self.entityType = .undefined
        }
        
        if let entityIdString = try values.decodeIfPresent(String.self, forKey: .entityId), let entityId = Int(entityIdString) {
            self.entityId = entityId
        } else {
            self.entityId = nil
        }
        
        self.entityName = try values.decodeIfPresent(String.self, forKey: .entityName)
    }
}
