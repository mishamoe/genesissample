//
//  TGAPNNotification.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

class TGAPNNotification: Codable {
    let title: String?
    let body: String?
    let actionButton: String?
    let arguments: [String]?
    let badge: Int?
    let sound: String?
    let customActions: [TGAPNAction]?
    let apnId: String?
    
    enum CodingKeys: String, CodingKey {
        case aps
        case customActions = "custom-actions"
        case apnId
        
        enum Aps: String, CodingKey {
            case alert
            case badge
            case sound
            
            enum Alert: String, CodingKey {
                case title
                case body
                case actionButton = "action-loc-key"
                case arguments = "loc-args"
            }
        }
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let aps = try container.nestedContainer(keyedBy: CodingKeys.Aps.self, forKey: .aps)
        let alert = try aps.nestedContainer(keyedBy: CodingKeys.Aps.Alert.self, forKey: .alert)
        
        title = try alert.decodeIfPresent(String.self, forKey: .title)
        body = try alert.decodeIfPresent(String.self, forKey: .body)
        actionButton = try alert.decodeIfPresent(String.self, forKey: .actionButton)
        arguments = try alert.decodeIfPresent([String].self, forKey: .arguments)
        
        badge = try aps.decodeIfPresent(Int.self, forKey: .badge)
        sound = try aps.decodeIfPresent(String.self, forKey: .sound)
        
        customActions = try container.decodeIfPresent([TGAPNAction].self, forKey: .customActions)
        apnId = try container.decodeIfPresent(String.self, forKey: .apnId)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        var aps = container.nestedContainer(keyedBy: CodingKeys.Aps.self, forKey: .aps)
        var alert = aps.nestedContainer(keyedBy: CodingKeys.Aps.Alert.self, forKey: .alert)
        
        try alert.encodeIfPresent(title, forKey: .title)
        try alert.encodeIfPresent(body, forKey: .body)
        try alert.encodeIfPresent(actionButton, forKey: .actionButton)
        try alert.encodeIfPresent(arguments, forKey: .arguments)

        try aps.encodeIfPresent(badge, forKey: .badge)
        try aps.encodeIfPresent(sound, forKey: .sound)

        try container.encodeIfPresent(customActions, forKey: .customActions)
        try container.encodeIfPresent(apnId, forKey: .apnId)
    }
}

extension TGAPNNotification {
    class func notification(from JSONDictionary: [AnyHashable: Any]) throws -> TGAPNNotification {
        let data = try JSONSerialization.data(withJSONObject: JSONDictionary, options: .prettyPrinted)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.utc)
        return try decoder.decode(TGAPNNotification.self, from: data)
    }
}
