//
//  TGCustomAction.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

enum TGCustomAction: Int, Codable {
    case undefined               = -1
    case push                    =  0  // navigates to given area and select given entity there
    case refresh                 =  1  // reloads given entity from the server
    case updateApplication       =  2  // initiates updating whole application "over the air"
    case immediateJob            =  4  // shows immediate job modal view
    case silentRefresh           =  5  // silent refresh
    case locationRequest         =  6  // ask for location update
    case travelMode              =  7  // changes given WO status to travel mode
    case onSiteMode              =  8  // changes given WO status to on-site mode
    case pointsReconciliation    =  9  // points approved notification
    
    case jobNotification         =  11 // changes given WO status to on-site mode
    case overdueJobNotification  =  12 // Overdue unassingned job on immediate list
    case FSRescheduleRequest     =  13 // reschedule request notification
    case newsNotification        =  14 // New mandatory news notification
    case ETANotification         =  15 // New pending ETA notification
    case lossFoundNotification   =  16 // Loss or found inventory notification for manager
    case IMSJobAssignment        =  17 // IMS job assignment notification
    case travelETANotification   =  18 // Travel ETA notification for tech after manager assigning
    case inventoryTransfer       =  20 // Received an inventory transfer notification
}
