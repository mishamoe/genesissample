//
//  TGEntityType.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

enum TGEntityType: Int, Codable {
    case undefined                 = -1
    
    case workOrder                 =  3 // WorkOrder (from TechGenie)
    case screen                    =  4 // Screen (from TechGenie)
    case tech                      =  6
    case transfer                  =  7 // Transfer (from TechGenie)
    case transferRequest           =  8 // Transfer Request (from TechGenie)
}
