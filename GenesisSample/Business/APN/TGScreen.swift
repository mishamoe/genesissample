//
//  TGScreen.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

enum TGScreen: Int, Codable {
    case teamSchedule                  =  1
    case preInstallSurvey              =  2
}
