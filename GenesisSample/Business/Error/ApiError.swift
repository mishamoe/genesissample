//
//  ApiError.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

/// Used for parsing generic backend errors
struct ApiError: Error, Codable {
    let httpStatusCode: Int
    let errorId: String
    let message: String
    let details: [String]?
}
