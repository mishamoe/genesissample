//
//  AuthError.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

enum AuthError: Error {
    case unauthorized
    case forbidden
    case tokenUnavailable
    case tokenInvalid
}

extension AuthError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .tokenUnavailable:
            return NSLocalizedString("Unable to obtain token. Please try again or contact administrator if the problem persists", comment: "")
        case .tokenInvalid:
            return NSLocalizedString("Token invalid. Please try again or contact administrator if the problem persists", comment: "")
        case .unauthorized:
            return NSLocalizedString("Unable to authenticate. Please try again or contact administrator if the problem persists", comment: "")
        case .forbidden:
            return NSLocalizedString("You don’t have permission to access this resource. Please contact administrator to request access", comment: "")
        }
    }
}
