//
//  ServiceError.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

enum ServiceError: Error {
    case badRequest(message: String?)
    case notFound
    case clientError
    case serverError
}

extension ServiceError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .badRequest(let message):
            if let errorMessage = message, !errorMessage.isEmpty {
                return NSLocalizedString("\(errorMessage)", comment: "")
            }
            return NSLocalizedString("Bad request", comment: "")
        case .notFound:
            return NSLocalizedString("Not found", comment: "")
        case .clientError:
            return NSLocalizedString("Client error", comment: "")
        case .serverError:
            return NSLocalizedString("Can't process the request", comment: "")
        }
    }
}
