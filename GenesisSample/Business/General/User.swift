//
//  User.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//


// TODO: Implementatino of TGAPNManager isn't a part of Sample Project
class User {
    static var current: User!
    var identifier: Int
    
    init(identifier: Int) {
        self.identifier = identifier
    }
}
