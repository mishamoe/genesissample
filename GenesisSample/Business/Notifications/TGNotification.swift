//
//  TGNotification.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/14/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

class TGNotification: Codable {
    var id: Int = 0
    var createdAt: Date!
    var isRead: Bool = false
    var typeId: Int = 0
    var apn: TGAPNNotification!
    
    var type: TGNotificationType {
        return TGNotificationType(rawValue: typeId) ?? .other
    }
    
    lazy var isActionable: Bool = {
        guard let customAction = apn.customActions?.first else { return false }
        
        switch customAction.action {
        case .push,
             .jobNotification,
             .overdueJobNotification,
             .FSRescheduleRequest,
             .lossFoundNotification:
            return true
        default:
            return false
        }
    }()
    
    private enum CodingKeys: String, CodingKey {
        case id
        case createdAt
        case isRead
        case typeId = "type"
        case apn
    }
}

extension TGNotification: Equatable {
    static func == (lhs: TGNotification, rhs: TGNotification) -> Bool {
        return lhs.id == rhs.id
    }
}

extension TGNotification {
    class func notification(from JSONDictionary: [AnyHashable: Any]) throws -> TGNotification {
        let data = try JSONSerialization.data(withJSONObject: JSONDictionary, options: .prettyPrinted)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.utc)
        return try decoder.decode(TGNotification.self, from: data)
    }
    
    class func notifications(from JSONArray: [Any]) throws -> [TGNotification] {
        let data = try JSONSerialization.data(withJSONObject: JSONArray, options: .prettyPrinted)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.utc)
        return try decoder.decode([TGNotification].self, from: data)
    }
}
