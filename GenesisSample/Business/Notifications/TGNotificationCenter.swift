//
//  TGNotificationCenter.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit
import UserNotifications

class TGNotificationCenter: NSObject {
    
    // MARK: - Properties (static)
    
    static var unreadNotificationsCount: Int = 0
    static var isLoadingUnreadNotificationsCount: Bool = false
    
    // MARK: - Properties (public)
    
    var notifications: [TGNotification] = []
    
    var filteredNotifications: [TGNotification] {
        return notifications
            .filter(filterByDate)
            .filter(filterByType)
            .filter(filterByStatus)
            .filter(filterBySearchQuery)
            .sortedByDate()
    }
    
    var searchQuery: String? {
        didSet {
            searchQuery = searchQuery?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    // MARK: - Properties (private)
    private let user: User
    private let apiService: APIService
    private let apnManager: TGAPNManager?
    
    private var filters: [TGNotificationsFilter: [TGNotificationsFilterOption: Bool]] = [
        .date: [.today: false, .yesterday: false, .last7Days: false, .last14Days: false, .all: true],
        .type: [.imsDispatches: true, .inventory: true, .jobAssignment: true, .rescheduleRequests: true, .other: true],
        .status: [.read: true, .unread: true]
    ]
    private var filtersCopy: [TGNotificationsFilter: [TGNotificationsFilterOption: Bool]] = [:]
    
    // MARK: - Initialization
    
    init(user: User, apiService: APIService, apnManager: TGAPNManager?) {
        self.user = user
        self.apiService = apiService
        self.apnManager = apnManager
    }
    
    // MARK: - Loading
    
    func loadNotifications(with completion: @escaping ([TGNotification]?, NSError?) -> Void) {
        apiService.loadNotifications(forTech: user.identifier) { [weak self] notifications, error in
            if let error = error {
                completion(nil, error as NSError)
                return
            }
            
            if let notifications = notifications {
                self?.notifications = notifications
                self?.updateUnreadNotificationsCount()
            }
            
            completion(self?.notifications, nil)
        }
    }
    
    @objc
    func loadUnreadNotificationsCount(with completion: @escaping (Int, NSError?) -> Void) {
        TGNotificationCenter.isLoadingUnreadNotificationsCount = true
        
        apiService.loadUnreadNotificationsCount(forTech: user.identifier) { data, error in
            let count = data as? Int ?? 0
            TGNotificationCenter.unreadNotificationsCount = count
            TGNotificationCenter.isLoadingUnreadNotificationsCount = false
            completion(count, error as NSError?)
        }
    }
    
    // MARK: - Actions
    
    func readNotification(withApnId apnId: String, completion: @escaping ([TGNotification]?, NSError?) -> Void) {
        apiService.readNotificationAndGetList(withApnId: apnId, tech: user.identifier) { [weak self] data, error in
            if let error = error {
                completion(nil, error as NSError)
                return
            }
            
            if let data = data as? [Any] {
                do {
                    self?.notifications = try TGNotification.notifications(from: data)
                    self?.updateUnreadNotificationsCount()
                } catch let error as NSError {
                    completion(nil, error)
                }
            }
            
            completion(self?.notifications, nil)
        }
    }
    
    func readNotifications(_ notifications: [TGNotification], completion: @escaping (NSError?) -> Void) {
        for notification in notifications {
            notification.isRead = true
        }
        updateUnreadNotificationsCount()
        
        let ids = notifications.compactMap { $0.apn.apnId }
        TGNotificationCenter.removeNotificationsFromSystemCenter(byIds: ids)
        
        let notificationIDs = notifications.map { $0.id }
        apiService.readNotifications(notificationIDs, tech: user.identifier) { _, error in
            completion(error as NSError?)
        }
    }
    
    func readAllNotifications(_ completion: @escaping (NSError?) -> Void) {
        for notification in notifications {
            notification.isRead = true
        }
        updateUnreadNotificationsCount()
        
        let ids = notifications.compactMap { $0.apn.apnId }
        TGNotificationCenter.removeNotificationsFromSystemCenter(byIds: ids)
        
        apiService.readAllNotifications(forTech: user.identifier) { _, error in
            completion(error as NSError?)
        }
    }
    
    func unreadNotifications(_ notifications: [TGNotification], completion: @escaping (NSError?) -> Void) {
        for notification in notifications {
            notification.isRead = false
        }
        updateUnreadNotificationsCount()
        
        let notificationIDs = notifications.map { $0.id }
        apiService.unreadNotifications(notificationIDs, tech: user.identifier) { _, error in
            completion(error as NSError?)
        }
    }
    
    func unreadAllNotifications(_ completion: @escaping (NSError?) -> Void) {
        for notification in notifications {
            notification.isRead = false
        }
        updateUnreadNotificationsCount()
        
        apiService.unreadAllNotifications(forTech: user.identifier) { _, error in
            completion(error as NSError?)
        }
    }
    
    func deleteNotifications(_ notifications: [TGNotification], completion: @escaping (NSError?) -> Void) {
        for notification in notifications {
            if let index = self.notifications.firstIndex(of: notification) {
                self.notifications.remove(at: index)
            }
        }
        updateUnreadNotificationsCount()
        
        let notificationIDs = notifications.map { $0.id }
        apiService.deleteNotifications(notificationIDs, tech: user.identifier) { _, error in
            completion(error as NSError?)
        }
    }
    
    func deleteAllNotifications(_ completion: @escaping (NSError?) -> Void) {
        notifications.removeAll()
        updateUnreadNotificationsCount()
        
        apiService.deleteAllNotifications(forTech: user.identifier) { _, error in
            completion(error as NSError?)
        }
    }
    
    func updateUnreadNotificationsCount() {
        TGNotificationCenter.unreadNotificationsCount = notifications.filter { !$0.isRead }.count
        // TODO: Not part of Sample Project
        //        ScreenManager.sharedInstance().mainMenuViewController?.updateUnreadNotificationsCount()
    }
    
    static func removeNotificationsFromSystemCenter(byIds ids: [String]) {
        UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
            var idsForRemove = [String]()
            for notification in notifications {
                let apn = try? TGAPNNotification.notification(from: notification.request.content.userInfo)
                
                if let apnId = apn?.apnId, ids.contains(apnId) {
                    idsForRemove.append(notification.request.identifier)
                }
            }
            if !idsForRemove.isEmpty {
                UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: idsForRemove)
            }
        }
    }
    
    // MARK: - Filters
    
    func doFiltersCopy() {
        filtersCopy = filters
    }
    
    func resetFiltersFromCopy() {
        filters = filtersCopy
    }
    
    func applyFilterOption(filter: TGNotificationsFilter, option: TGNotificationsFilterOption, selected: Bool) {
        switch filter {
        case .date:
            // User can check only one date at a time, so previously chosen one will be unchecked.
            filters[filter]?.forEach {
                filters[filter]?[$0.key] = false
            }
            
            filters[filter]?[option] = true
        case .type, .status:
            let isOptionSelected = filters[filter]?[option] ?? false
            let allOptionsCount = filters[filter]?.count ?? 0
            let deselectedOptionsCount = filters[filter]?.filter { !$0.value }.count ?? 0
            let isLastSelectedOption = isOptionSelected && (deselectedOptionsCount == allOptionsCount - 1)
            
            // User can't deselect last selected option. At least one option will always remain selected.
            if !selected && isLastSelectedOption {
                filters[filter]?[option] = true
            } else {
                filters[filter]?[option] = selected
            }
        }
    }
    
    func isFilterOptionSelected(filter: TGNotificationsFilter, option: TGNotificationsFilterOption) -> Bool {
        return filters[filter]?[option] ?? false
    }
    
    private func filterByDate(_ notification: TGNotification) -> Bool {
        // Notifications without dates should be displayed regardless of this filter
        guard let createdAt = notification.createdAt else { return true }
        
        guard let dateFilter = filters[TGNotificationsFilter.date],
            let todayOption = dateFilter[.today],
            let yesterdayOption = dateFilter[.yesterday],
            let lastWeekOption = dateFilter[.last7Days],
            let lastTwoWeeksOption = dateFilter[.last14Days],
            let allOption = dateFilter[.all] else {
                fatalError("Notifications Center: Date Filter isn't defined correctly")
        }
        
        if todayOption {
            return createdAt.isToday()
        } else if yesterdayOption {
            return createdAt.isYesterday()
        } else if lastWeekOption {
            return createdAt.isLaterThan7DaysAgo()
        } else if lastTwoWeeksOption {
            return createdAt.isLaterThan14DaysAgo()
        } else if allOption {
            return true
        }
        
        return true
    }
    
    private func filterByType(_ notification: TGNotification) -> Bool {
        guard let typeFilter = filters[TGNotificationsFilter.type],
            let imsDispatchesOption = typeFilter[.imsDispatches],
            let inventoryOption = typeFilter[.inventory],
            let jobAssignmentOption = typeFilter[.jobAssignment],
            let rescheduleRequestsOption = typeFilter[.rescheduleRequests],
            let otherOption = typeFilter[.other] else {
                fatalError("Notifications Center: Type Filter isn't defined correctly")
        }
        
        switch notification.type {
        case .imsDispatches where imsDispatchesOption:
            return true
        case .inventory where inventoryOption:
            return true
        case .jobAssignment where jobAssignmentOption:
            return true
        case .rescheduleRequests where rescheduleRequestsOption:
            return true
        case .other where otherOption:
            return true
        default:
            return false
        }
    }
    
    private func filterByStatus(_ notification: TGNotification) -> Bool {
        guard let statusFilter = filters[TGNotificationsFilter.status],
            let readOption = statusFilter[.read],
            let unreadOption = statusFilter[.unread] else {
                fatalError("Notifications Center: Status Filter isn't defined correctly")
        }
        
        if notification.isRead, readOption {
            return true
        } else if !notification.isRead, unreadOption {
            return true
        } else {
            return false
        }
    }
    
    private func filterBySearchQuery(_ notification: TGNotification) -> Bool {
        guard let searchString = searchQuery,
            let notificationBody = notification.apn.body,
            !searchString.isEmpty,
            !notificationBody.isEmpty else {
                return true
        }
        
        return notificationBody.lowercased().contains(searchString.lowercased())
    }
    
    // MARK: - Actions
    
    func processNotification(_ notification: TGNotification) {
        guard notification.isActionable else {
            return
        }
        guard let apn = notification.apn else {
            return
        }
        
        createAlertForNotification(apn) {
            // TODO: Processing of notifications is not  a part of Sample Project
        }
    }
    
    private func createAlertForNotification(_ notification: TGAPNNotification, completion: @escaping () -> Void) {
        guard let topViewController = topViewController() else { return }
        
        let message = notification.body
        let title = notification.title ?? notification.arguments?.first
        let buttonTitle = notification.actionButton
        
        let alertController = TGAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let confirmAction = UIAlertAction(title: buttonTitle, style: .default) { _ in
            completion()
        }
        alertController.addAction(confirmAction)
        
        topViewController.present(alertController, animated: true, completion: nil)
    }
    
    private func topViewController() -> UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
}

extension Array where Element == TGNotification {
    func sortedByDate() -> [TGNotification] {
        var notifications = filter { $0.createdAt != nil }
            .sorted { $0.createdAt > $1.createdAt }
        let withoutDate = filter { $0.createdAt == nil }
        notifications.append(contentsOf: withoutDate)
        
        return notifications
    }
}


