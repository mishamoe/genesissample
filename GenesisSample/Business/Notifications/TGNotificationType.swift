//
//  NotificationType.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/14/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

enum TGNotificationType: Int {
    case other = 0
    case rescheduleRequests = 1
    case imsDispatches = 2
    case inventory = 3
    case jobAssignment = 5
    
    var title: String {
        switch self {
        case .other:
            return NSLocalizedString("Other", comment: "")
        case .rescheduleRequests:
            return NSLocalizedString("Reschedule Requests", comment: "")
        case .imsDispatches:
            return NSLocalizedString("IMS Dispatches", comment: "")
        case .inventory:
            return NSLocalizedString("Inventory", comment: "")
        case .jobAssignment:
            return NSLocalizedString("Job Assignment", comment: "")
        }
    }
}
