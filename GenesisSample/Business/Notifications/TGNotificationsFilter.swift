//
//  TGNotificationsFilter.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

enum TGNotificationsFilter: Int {
    case date, type, status
    
    var title: String {
        switch self {
        case .date: return NSLocalizedString("By date", comment: "")
        case .type: return NSLocalizedString("By type", comment: "")
        case .status: return NSLocalizedString("By status", comment: "")
        }
    }
}
