//
//  TGNotificationsFilterOption.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

enum TGNotificationsFilterOption {
    case today, yesterday, last7Days, last14Days, all
    case imsDispatches, inventory, rescheduleRequests, jobAssignment, other
    case read, unread
    
    var title: String {
        switch self {
        case .today: return NSLocalizedString("Today", comment: "")
        case .yesterday: return NSLocalizedString("Yesterday", comment: "")
        case .last7Days: return NSLocalizedString("Last 7 days", comment: "")
        case .last14Days: return NSLocalizedString("Last 14 days", comment: "")
        case .all: return NSLocalizedString("All (last 30 days)", comment: "")
            
        case .imsDispatches: return NSLocalizedString("IMS Dispatches", comment: "")
        case .inventory: return NSLocalizedString("Inventory", comment: "")
        case .rescheduleRequests: return NSLocalizedString("Reschedule Requests", comment: "")
        case .jobAssignment: return NSLocalizedString("Job Assignment", comment: "")
        case .other: return NSLocalizedString("Other", comment: "")
            
        case .read: return NSLocalizedString("Read", comment: "")
        case .unread: return NSLocalizedString("Unread", comment: "")
        }
    }
}
