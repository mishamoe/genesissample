//
//  AppDelegate+Additions.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

extension AppDelegate {
    class var shared: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
}
