//
//  DataRequest+Additions.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation
import Alamofire

extension DataRequest {
    func validateResponse() -> Self {
        return validate { _, response, data -> Request.ValidationResult in
            let decoder = JSONDecoder()
            if let data = data, let apiError = try? decoder.decode(ApiError.self, from: data) {
                return .failure(apiError)
            }
            
            switch response.statusCode {
            case 400:
                var errorMessage: String?
                if let data = data,
                    let dict = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any] {
                    errorMessage = dict["message"] as? String
                }
                return .failure(ServiceError.badRequest(message: errorMessage))
            case 401:
                return .failure(AuthError.unauthorized)
            case 403:
                return .failure(AuthError.forbidden)
            case 404:
                return .failure(ServiceError.notFound)
            case 405...499:
                return .failure(ServiceError.clientError)
            case 500...599:
                return .failure(ServiceError.serverError)
            default:
                return .success
            }
        }
    }
    
    /// Adds a handler to be called once the request has finished.
    ///
    /// - Parameters:
    ///   - queue:             The queue on which the completion handler is dispatched. Defaults to `nil`, which means
    ///                        the handler is called on `.main`.
    ///   - decoder:           The decoder to use to decode the response. Defaults to a `JSONDecoder` with default
    ///                        settings.
    ///   - completionHandler: A closure to be executed once the request has finished.
    /// - Returns:             The request.
    @discardableResult
    public func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil,
                                                decoder: JSONDecoder = JSONDecoder(),
                                                completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        decoder.dateDecodingStrategy = .formatted(DateFormatter.utc)
        return responseData(queue: queue) { response in
            let decoded = response.flatMap { data in
                try decoder.decode(T.self, from: data)
            }
            completionHandler(decoded)
        }
    }
}

