//
//  Date+Additions.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

public extension Date {
    static func dateTomorrow() -> Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date())!
    }
    
    static func dateYestarday() -> Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date())!
    }
    
    func isEqualToDateIgnoringTime(_ date: Date) -> Bool {
        let components1 = Calendar.current.dateComponents([.year, .month, .day], from: self)
        let components2 = Calendar.current.dateComponents([.year, .month, .day], from: date)
        return components1.year == components2.year &&
            components1.month == components2.month &&
            components1.day == components2.day
    }
    
    func isToday() -> Bool {
        return isEqualToDateIgnoringTime(Date())
    }
    
    func isYesterday() -> Bool {
        return isEqualToDateIgnoringTime(Date.dateYestarday())
    }
    
    func isLaterThanYesterday() -> Bool {
        let yesterdayDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        let yesterdayStartOfDayDate = Calendar.current.startOfDay(for: yesterdayDate)
        return compare(yesterdayStartOfDayDate) == .orderedDescending
    }
    
    func isLaterThan7DaysAgo() -> Bool {
        let lastWeekDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
        let lastWeekStartOfDayDate = Calendar.current.startOfDay(for: lastWeekDate)
        return compare(lastWeekStartOfDayDate) == .orderedDescending
    }
    
    func isLaterThan14DaysAgo() -> Bool {
        let lastTwoWeeksDate = Calendar.current.date(byAdding: .day, value: -14, to: Date())!
        let lastTwoWeeksStartOfDayDate = Calendar.current.startOfDay(for: lastTwoWeeksDate)
        return compare(lastTwoWeeksStartOfDayDate) == .orderedDescending
    }
}
