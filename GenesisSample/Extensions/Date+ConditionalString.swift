//
//  Date+ConditionalString.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

extension Date {
    var conditionalString: String {
        let calendar = Calendar.current
        var output = ""
        
        let dateStartOfDay = calendar.startOfDay(for: self)
        let todayStartOfDay = calendar.startOfDay(for: Date())
        
        let unitFlags: NSCalendar.Unit = [.month, .weekOfYear, .year, .day]
        let dateComponents = (calendar as NSCalendar).components(unitFlags,
                                                                 from: dateStartOfDay,
                                                                 to: todayStartOfDay,
                                                                 options: .wrapComponents)

        if dateComponents.year == 0 && dateComponents.month == 0 && dateComponents.weekOfYear == 0 && dateComponents.day ?? 0 < 2 {
            if dateComponents.day == 1 {
                output += NSLocalizedString("Yesterday", comment: "")
            } else {
                output += NSLocalizedString("Today", comment: "")
            }
        } else {
            let dateWithoutYear = DateFormatter.dateWithoutYear.string(from: self)
            
            output += dateWithoutYear
        }
        
        return output
    }
}
