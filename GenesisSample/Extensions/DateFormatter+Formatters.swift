//
//  DateFormatter+Formatters.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/14/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

public extension DateFormatter {
    static let utc: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter
    }()
    
    static let dateWithoutYear: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d"
        return formatter
    }()
}
