//
//  Decodable+JSON.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

extension Decodable {
    static func object(fromJSONObject json: Any) throws -> Self {
        let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let decoder = JSONDecoder()
        return try decoder.decode(Self.self, from: data)
    }
    
    static func array(fromJSONArray json: Any) throws -> [Self] {
        let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let decoder = JSONDecoder()
        return try decoder.decode([Self].self, from: data)
    }
}
