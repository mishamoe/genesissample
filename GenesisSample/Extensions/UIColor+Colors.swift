//
//  UIColor+Colors.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

extension UIColor {
    class var tgOrange: UIColor {
        return UIColor(red: 253.0/255.0, green: 130.0/255.0, blue: 4.0/255.0, alpha: 1.0)
    }
    
    class var tgText: UIColor {
        return UIColor(red: 48.0/255.0, green: 48.0/255.0, blue: 48.0/255.0, alpha: 1.0)
    }
    
    class var tgDarkGrayBg: UIColor {
        return UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
    }
    
    class var tgWhite: UIColor {
        return .white
    }
    
    class var tgCellHighlightedBg: UIColor {
        return .tgOrange
    }
    
    class var tgCellText: UIColor {
        return .tgText
    }
    
}
