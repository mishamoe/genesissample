//
//  UIFont+Fonts.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

enum FontSize: CGFloat {
    case extraBig        = 36.0
    case large           = 24.0
    case title           = 18.0
    case subtitle        = 15.0
    case details         = 12.0
    case description     = 13.0
    case subdetails      = 10.0
    case barButtonItem   = 17.0
}

extension UIFont {
    class var tgSubtitle: UIFont {
        return .systemFont(ofSize: FontSize.subtitle.rawValue)
    }
    
    class func tgFont(ofSize size: CGFloat) -> UIFont {
        return .systemFont(ofSize: size)
    }
    
    class func tgSemiboldFont(ofSize size: CGFloat) -> UIFont {
        return .systemFont(ofSize: size, weight: .semibold)
    }
    
    class func tgBoldFont(ofSize size: CGFloat) -> UIFont {
        return .systemFont(ofSize: size, weight: .bold)
    }
}
