//
//  UILabel+AttributedText.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

extension UILabel {
    func setAttributedText(_ text: String, withColor color: UIColor) {
        guard !text.isEmpty else {
            return
        }
        
        var subAttributes = attributedText?.attributes(at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: text.count))
        subAttributes?[NSAttributedString.Key.foregroundColor] = color
        
        guard let selfText = self.text?.lowercased() as NSString? else {
            return
        }
        let range = selfText.range(of: text.lowercased())
        
        let attributedText = NSMutableAttributedString(string: self.text ?? "", attributes: nil)
        attributedText.setAttributes(subAttributes, range: range)
        
        self.attributedText = attributedText
    }
}
