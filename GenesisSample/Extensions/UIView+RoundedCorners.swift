//
//  UIView+RoundedCorners.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let cornerRadii = CGSize(width: radius, height: radius)
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: cornerRadii)
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }
    
    func roundTopCornersRadius(_ radius: CGFloat) {
        roundCorners([.topLeft, .topRight], radius: radius)
    }
    
    func roundBottomCornersRadius(_ radius: CGFloat) {
        roundCorners([.bottomLeft, .bottomRight], radius: radius)
    }
}
