//
//  UIViewController+DisplayError.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/17/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

extension UIViewController {
    func displayError(_ errorMessage: String) {
        let alert = TGAlertController(title: NSLocalizedString("Error", comment: ""),
                                      message: errorMessage,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                      style: .default,
                                      handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
