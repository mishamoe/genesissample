//
//  APIResponse.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation



class APIResponse<T>: Codable where T: Codable {
    let data: T
    let errorCode: Int?
    let message: String
    let success: Bool
}
