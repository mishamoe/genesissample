//
//  APIService.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation
import Alamofire

class APIService: NotificationsAPIService {
    // serviceUrl was replaced for Sample Project. Logic with different environments was removed.
    private let serviceUrl = "https://sample-project.com/api"
    
    func loadNotifications(forTech tech: Int, completion: @escaping ([TGNotification]?, Error?) -> Void) {
        let url = "\(serviceUrl)/Notification/NotificationList"
        let parameters = ["employeeId": tech]
        
        Alamofire
            .request(url, method: .get, parameters: parameters)
            .validateResponse()
            .responseDecodable { (response: DataResponse<APIResponse<[TGNotification]>>) in
                switch response.result {
                case .success(let result):
                    completion(result.data, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
    }
    
    func loadUnreadNotificationsCount(forTech tech: Int, completion: (Any?, Error?) -> Void) {
        // TODO: Not part of Sample Project
        completion(nil, nil)
    }
    
    func readNotificationAndGetList(withApnId apnId: String, tech: Int, completion: (Any?, Error?) -> Void) {
        // TODO: Not part of Sample Project
        completion(nil, nil)
    }
    
    func readNotifications(_ notificationIds: [Int], tech: Int, completion: (Any?, Error?) -> Void) {
        // TODO: Not part of Sample Project
        completion(nil, nil)
    }
    
    func readAllNotifications(forTech tech: Int, completion: (Any?, Error?) -> Void) {
        // TODO: Not part of Sample Project
        completion(nil, nil)
    }
    
    func unreadNotifications(_ notificationIds: [Int], tech: Int, completion: (Any?, Error?) -> Void) {
        // TODO: Not part of Sample Project
        completion(nil, nil)
    }
    
    func unreadAllNotifications(forTech tech: Int, completion: (Any?, Error?) -> Void) {
        // TODO: Not part of Sample Project
        completion(nil, nil)
    }
    
    func deleteNotifications(_ notificationIds: [Int], tech: Int, completion: (Any?, Error?) -> Void) {
        // TODO: Not part of Sample Project
        completion(nil, nil)
    }
    
    func deleteAllNotifications(forTech tech: Int, completion: (Any?, Error?) -> Void) {
        // TODO: Not part of Sample Project
        completion(nil, nil)
    }
    
}
