//
//  NotificationsAPIService.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

protocol NotificationsAPIService {
    func loadNotifications(forTech tech: Int, completion: @escaping ([TGNotification]?, Error?) -> Void)
    func loadUnreadNotificationsCount(forTech tech: Int, completion: (Any?, Error?) -> Void)
    func readNotificationAndGetList(withApnId apnId: String, tech: Int, completion: (Any?, Error?) -> Void)
    func readNotifications(_ notificationIds: [Int], tech: Int, completion: (Any?, Error?) -> Void)
    func readAllNotifications(forTech tech: Int, completion: (Any?, Error?) -> Void)
    func unreadNotifications(_ notificationIds: [Int], tech: Int, completion: (Any?, Error?) -> Void)
    func unreadAllNotifications(forTech tech: Int, completion: (Any?, Error?) -> Void)
    func deleteNotifications(_ notificationIds: [Int], tech: Int, completion: (Any?, Error?) -> Void)
    func deleteAllNotifications(forTech tech: Int, completion: (Any?, Error?) -> Void)
}
