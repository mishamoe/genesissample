//
//  AppServices.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

/// Dependency container for the app
final class AppServices: ServiceProvider {
    var api: APIService
    var stubs: StubsService?
    var notificationCenter: TGNotificationCenter
    var apnManager: TGAPNManager
    
    init(user: User) {
        api = APIService()
        
        // Used to provide stub data
        stubs = StubsService()
        
        apnManager = TGAPNManager()
        notificationCenter = TGNotificationCenter(user: user, apiService: api, apnManager: apnManager)
    }
}
