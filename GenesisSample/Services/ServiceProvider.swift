//
//  ServiceProvider.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/15/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

protocol ServiceProvider {
    var api: APIService { get set }
    var stubs: StubsService? { get set }
    var notificationCenter: TGNotificationCenter { get set }
    var apnManager: TGAPNManager { get set }
}
