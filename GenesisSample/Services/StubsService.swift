//
//  StubsService.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

class StubsService {
    init() {
        addStub(verb: HTTPMethod.get,
                pattern: "Notification/NotificationList",
                resource: "NotificationList.json",
                bundle: Bundle(for: type(of: self)))
    }
}
