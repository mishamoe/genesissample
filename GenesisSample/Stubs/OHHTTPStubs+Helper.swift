//
//  OHHTTPStubs+Helper.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation
import OHHTTPStubs

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

enum HTTPStatus: Int32 {
    case ok = 200 // RFC 7231, 6.3.1 //swiftlint:disable:this identifier_name
    case created = 201 // RFC 7231, 6.3.2
    case accepted = 202 // RFC 7231, 6.3.3
    case noContent = 204 // RFC 7231, 6.3.5
    
    case multipleChoices = 300 // RFC 7231, 6.4.1
    case movedPermanently = 301 // RFC 7231, 6.4.2
    case found = 302 // RFC 7231, 6.4.3
    case notModified = 304 // RFC 7232, 4.1
    
    case badRequest = 400 // RFC 7231, 6.5.1
    case unauthorized = 401 // RFC 7235, 3.1
    case forbidden = 403 // RFC 7231, 6.5.3
    case notFound = 404 // RFC 7231, 6.5.4
    case methodNotAllowed = 405 // RFC 7231, 6.5.5
    case notAcceptable = 406 // RFC 7231, 6.5.6
    case requestTimeout = 408 // RFC 7231, 6.5.7
    case conflict = 409 // RFC 7231, 6.5.8
    case gone = 410 // RFC 7231, 6.5.9
    case requestEntityTooLarge = 413 // RFC 7231, 6.5.11
    case requestURITooLong = 414 // RFC 7231, 6.5.12
    case unsupportedMediaType = 415 // RFC 7231, 6.5.13
    case locked = 423 // RFC 4918, 11.3
    case tooManyRequests = 429 // RFC 6585, 4
    case requestHeaderFieldsTooLarge = 431 // RFC 6585, 5
    
    case internalServerError = 500 // RFC 7231, 6.6.1
    case notImplemented = 501 // RFC 7231, 6.6.2
    case badGateway = 502 // RFC 7231, 6.6.3
    case serviceUnavailable = 503 // RFC 7231, 6.6.4
}

let kDefaultContentType = ["Content-Type": "application/json"]
let kRSSContentType = ["Content-Type": "application/rss+xml"]
let kExtendedStubsLogging = true

private func regiterStub(verb: String,
                         pattern: String,
                         response: Data,
                         status: Int32,
                         headers: [String: String]?) -> OHHTTPStubsDescriptor? {
    
    let stubCondition: OHHTTPStubsTestBlock = { request -> Bool in
        if kExtendedStubsLogging {
            let testedUrl = request.url?.absoluteString ?? ""
            let testedVerb = request.httpMethod ?? ""
            print("❓ 📦 Test [\(testedVerb) \(testedUrl)] <~> [\(verb) \(pattern)]")
        }
        let sameVerb = request.httpMethod?.lowercased() == verb.lowercased()
        let containsPattern = request.url?.absoluteString.lowercased()
            .contains(pattern.lowercased()) ?? false
        let apply = sameVerb && containsPattern
        if kExtendedStubsLogging && apply {
            print("✅ 📦  \(status) [\(verb) \(pattern)]")
        }
        return apply
    }
    
    let headers = headers ?? kDefaultContentType
    let stubResponse: OHHTTPStubsResponseBlock = { _ -> OHHTTPStubsResponse in
        return OHHTTPStubsResponse(data: response,
                                   statusCode: status,
                                   headers: headers)
    }
    
    let descriptor = stub(condition: stubCondition, response: stubResponse)
    return descriptor
}

// MARK: - Special for Swift

@discardableResult
func addStub(verb: HTTPMethod,
             pattern: String,
             resource: String,
             bundle: Bundle,
             status: HTTPStatus = .ok,
             headers: [String: String]? = nil) -> OHHTTPStubsDescriptor? {
    guard let url = bundle.url(forResource: resource, withExtension: nil),
        let data = try? Data(contentsOf: url) else {
            print("❗️ 📦 Cannot read \(resource)")
            return nil
    }
    return regiterStub(verb: verb.rawValue,
                       pattern: pattern,
                       response: data,
                       status: status.rawValue,
                       headers: headers)
}

@discardableResult
func addRSSStub(verb: HTTPMethod,
                pattern: String,
                resource: String,
                bundle: Bundle,
                status: HTTPStatus = .ok) -> OHHTTPStubsDescriptor? {
    return addStub(verb: verb, pattern: pattern, resource: resource, bundle: bundle, status: status, headers: kRSSContentType)
}

@discardableResult
func addStub(verb: HTTPMethod,
             pattern: String,
             response: Any,
             status: HTTPStatus = .ok) -> OHHTTPStubsDescriptor? {
    guard let data = try? JSONSerialization.data(withJSONObject: response, options: []) else {
        print("❗️ 📦 Cannot convert response to JSON \(response)")
        return nil
    }
    return regiterStub(verb: verb.rawValue,
                       pattern: pattern,
                       response: data,
                       status: status.rawValue,
                       headers: kDefaultContentType)
}

@discardableResult
func addStub(withInfo info: StubInfo) -> OHHTTPStubsDescriptor? {
    let descriptor = regiterStub(verb: info.method.rawValue,
                                 pattern: info.path,
                                 response: info.content,
                                 status: info.statusCode.rawValue,
                                 headers: info.headers)!
    return descriptor
}

func removeStub(_ stub: OHHTTPStubsDescriptor) {
    OHHTTPStubs.removeStub(stub)
}

