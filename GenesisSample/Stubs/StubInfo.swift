//
//  StubInfo.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import Foundation

/// A container for creating stub for backend HTTP service
/// Main purposes are:
///    * to store and validate stub information like HTTP verb, partial path (including parameters)
///    * Serialize / Deserialize stub information into string for passing this information from test bundle into application bundle
/// Can be used for development UI tests
///
/// Examples of serialized StubInfo
///    - Key:   "HTTPSTUB|GET|/api/todos"
///    - Value: "200|encoded-json"
struct StubInfo {
    
    // MARK: - constants
    static let kUseHttpStubs = "useHTTPStubs"
    static let kHttpStubHeader = "HTTPSTUB"
    static let kSeparator = Character("|")
    
    // MARK: - variables
    let method: HTTPMethod
    let path: String
    let statusCode: HTTPStatus
    var headers: [String: String] = kDefaultContentType
    let content: Data
    var contentJSON: Any {
        guard let json = try? JSONSerialization.jsonObject(with: content, options: []) else {
            print("❗️ 📦 Cannot convert content data into JSON representaion")
            return [:]
        }
        return json
    }
    var contentString: String {
        guard let string = String(data: content, encoding: .utf8) else {
            print("❗️ 📦 Cannot convert content data into String representaion")
            return ""
        }
        return string
    }
    var key: String {
        var items = [String]()
        items.append(StubInfo.kHttpStubHeader)
        items.append(method.rawValue)
        items.append(path)
        return items.joined(separator: String(StubInfo.kSeparator))
    }
    
    var value: String {
        var items = [String]()
        items.append(String(statusCode.rawValue))
        items.append(contentString)
        return items.joined(separator: String(StubInfo.kSeparator))
    }
    
    /// Defines does given key is StubInfo or not
    static func isStubInfo(_ key: String) -> Bool {
        return key.hasPrefix(kHttpStubHeader)
    }
    
    // MARK: - initializers
    
    /// Constructs StubInfo from environment key:value strings
    /// Expects key as String with format:   "HTTPSTUB|GET|/api/todos"
    /// Expects value as String with format: "200|encoded-json"
    init?(environmentKey key: String, value: String) {
        let keyParts = key.split(separator: StubInfo.kSeparator)
        let valueParts = value.split(separator: StubInfo.kSeparator)
        guard keyParts.count == 3, valueParts.count == 2,
            let stub = keyParts.first, stub == StubInfo.kHttpStubHeader,
            let httpMethod = HTTPMethod(rawValue: String(keyParts[1])),
            let code = Int32(valueParts[0]) else { return nil }
        guard let httpStatus = HTTPStatus(rawValue: code) else {
            print("❗️ 📦 Unknown HTTP status code: \(code)")
            return nil
        }
        self.method = httpMethod
        self.path = String(keyParts[2])
        self.statusCode = httpStatus
        let contentAsString = String(valueParts[1])
        guard let data = contentAsString.data(using: .utf8) else {
            print("❗️ 📦 Cannot convert content into Data: \(contentAsString)")
            return nil
        }
        self.content = data
    }
    
    /// Constructs StubInfo with given JSON response
    /// Can return nil if any problems with serialization response into JSON
    /// @param method The Verb of HTTP request. Default GET
    /// @param path The partial path of the request we are going to stub
    /// @param statusCode The HTTP status code. Default 200 OK
    /// @param json Expected response as Dictionary or Array of objects/Dictionaries
    init?(method: HTTPMethod = .get, path: String, statusCode: HTTPStatus = .ok, json: Any) {
        self.method = method
        self.path = path
        self.statusCode = statusCode
        guard let data = try? JSONSerialization.data(withJSONObject: json, options: []) else {
            print("❗️ 📦 Cannot convert content from JSON: \(json)")
            return nil
        }
        self.content = data
    }
    
    /// Constructs StubInfo with response as resource file in bundle
    /// Can return nil if resource was not found in bundle
    /// @param method The Verb of HTTP request. Default GET
    /// @param path The partial path of the request we are going to stub
    /// @param statusCode The HTTP status code. Default 200 OK
    /// @param fileName The name of the resource. Should contain extension. Like "Rep.1234.json"
    init?(method: HTTPMethod = .get, path: String, statusCode: HTTPStatus = .ok, fileName: String, bundle: Bundle) {
        self.method = method
        self.path = path
        self.statusCode = statusCode
        guard let url = bundle.url(forResource: fileName, withExtension: nil),
            let data = try? Data(contentsOf: url) else {
                print("❗️ 📦 Cannot obtain data content from resource: \(fileName)")
                return nil
        }
        self.content = data
    }
}

// MARK: - CustomStringConvertible
extension StubInfo: CustomStringConvertible {
    var description: String {
        return "ℹ️ 📦 (\(statusCode)) \(method.rawValue) \(path)"
    }
}

