//
//  NotificationCell.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak private var unreadIndicatorView: UIView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var detailLabel: UILabel!
    @IBOutlet weak private var dateLabel: UILabel!
    @IBOutlet weak private var disclosureIndicatorImageView: UIImageView!
    
    private var searchTerm: String?
    
    var topCornerRadius: CGFloat?
    var bottomCornerRadius: CGFloat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    override func layoutSubviews() {
        layer.mask = nil
        super.layoutSubviews()
        if let topCornerRadius = topCornerRadius {
            roundTopCornersRadius(topCornerRadius)
        } else if let bottomCornerRadius = bottomCornerRadius {
            roundBottomCornersRadius(bottomCornerRadius)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        unreadIndicatorView.isHidden = true
        titleLabel.text = ""
        detailLabel.text = ""
        dateLabel.text = ""
        disclosureIndicatorImageView.isHidden = true
        topCornerRadius = nil
        bottomCornerRadius = nil
        // Remove mask which could contain rounded corners
        layer.mask = nil
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        unreadIndicatorView.backgroundColor = .tgOrange
        titleLabel.textColor = .black
        detailLabel.textColor = .tgText
        dateLabel.textColor = .tgDarkGrayBg
        highlightSerchTerm()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        if highlighted {
            selectedBackgroundView?.backgroundColor = UIColor.tgCellHighlightedBg
            unreadIndicatorView.backgroundColor = UIColor.tgWhite
            titleLabel.textColor = UIColor.tgWhite
            detailLabel.textColor = UIColor.tgWhite
            dateLabel.textColor = UIColor.tgWhite
            disclosureIndicatorImageView.tintColor = UIColor.tgWhite
        } else {
            selectedBackgroundView?.backgroundColor = UIColor.tgWhite
            unreadIndicatorView.backgroundColor = UIColor.tgOrange
            titleLabel.textColor = .black
            detailLabel.textColor = .tgText
            dateLabel.textColor = .tgDarkGrayBg
            disclosureIndicatorImageView.tintColor = .tgOrange
        }
        highlightSerchTerm()
    }
    
    private func configureUI() {
        tintColor = UIColor.tgOrange
        
        unreadIndicatorView.layer.cornerRadius = unreadIndicatorView.frame.size.width / 2
        titleLabel.font = UIFont.tgSemiboldFont(ofSize: FontSize.description.rawValue)
        titleLabel.textColor = .black
        detailLabel.font = UIFont.tgSubtitle
        detailLabel.textColor = UIColor.tgText
        dateLabel.font = UIFont.tgSemiboldFont(ofSize: FontSize.description.rawValue)
        dateLabel.textColor = UIColor.tgDarkGrayBg
        disclosureIndicatorImageView.tintColor = UIColor.tgOrange
        disclosureIndicatorImageView.isHidden = true
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .tgWhite
        selectedBackgroundView = backgroundView
    }
    
    private func highlightSerchTerm() {
        if let searchTerm = searchTerm, !searchTerm.isEmpty {
            detailLabel.setAttributedText(searchTerm, withColor: isHighlighted ? .tgText : .tgOrange)
        } else {
            detailLabel.textColor = isHighlighted ? .tgWhite : .tgText
        }
    }
    
    func configure(with  notification: TGNotification, searchTerm: String?) {
        self.searchTerm = searchTerm
        unreadIndicatorView.isHidden = notification.isRead
        titleLabel.text = notification.type.title.uppercased()
        detailLabel.text = notification.apn.body
        
        if let createdAt = notification.createdAt {
            dateLabel.text = createdAt.conditionalString
        } else {
            dateLabel.text = NSLocalizedString("N/A", comment: "")
        }
        
        disclosureIndicatorImageView.isHidden = !notification.isActionable
    }
    
    func markAsRead(_ isRead: Bool, completion: @escaping (Bool) -> Void) {
        UIView.transition(with: self.unreadIndicatorView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.unreadIndicatorView.isHidden = isRead
        }, completion: completion)
    }
}
