//
//  NotificationsFiltersViewController.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

protocol NotificationsFiltersViewControllerDelegate: class {
    func didUpdateFilters()
}

class NotificationsFiltersViewController: UITableViewController {
    
    weak var delegate: NotificationsFiltersViewControllerDelegate?
    var notificationCenter: TGNotificationCenter!
    
    var filters: [[TGNotificationsFilterOption]] = [
        [.today, .yesterday, .last7Days, .last14Days, .all],
        [.imsDispatches, .inventory, .jobAssignment, .rescheduleRequests, .other],
        [.read, .unread]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        notificationCenter.doFiltersCopy()
    }
    
    func configureView() {
        title = NSLocalizedString("Filters", comment: "")
        
        setupBarButtons()
        
        tableView.register(UINib(nibName: String(describing: SelectItemCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SelectItemCell.self))
    }
    
    func setupBarButtons() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonTouched))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTouched))
    }
    
    // MARK: - Actions
    
    @objc
    func cancelButtonTouched() {
        notificationCenter.resetFiltersFromCopy()
        dismiss(animated: true)
    }
    
    @objc
    func doneButtonTouched() {
        dismiss(animated: true) { [weak self] in
            self?.delegate?.didUpdateFilters()
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return filters.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filters[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let filter = TGNotificationsFilter(rawValue: section) else { return nil }
        return filter.title
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelectItemCell.self), for: indexPath) as? SelectItemCell else {
            return UITableViewCell()
        }
        // TODO:
        guard let filter = TGNotificationsFilter(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        let option = filters[indexPath.section][indexPath.row]

        cell.titleLabel.text = option.title
        cell.titleLabel.textAlignment = .left
        cell.checkmarkImageView.isHidden = !notificationCenter.isFilterOptionSelected(filter: filter, option: option)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let filter = TGNotificationsFilter(rawValue: indexPath.section) else { return }
        let option = filters[indexPath.section][indexPath.row]
        let isSelected = notificationCenter.isFilterOptionSelected(filter: filter, option: option)
        notificationCenter.applyFilterOption(filter: filter, option: option, selected: !isSelected)
        tableView.reloadData()
    }
    
}

