//
//  NotificationsViewController.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

private enum Constants {
    static let tableViewCornerRadius: CGFloat = 10
    static let filtersModalViewContentSize = CGSize(width: 400.0, height: 618.0)
    
    static let title = NSLocalizedString("Notifications", comment: "")
    
    static let deleteActionTitle = NSLocalizedString("Delete", comment: "")
    static let readActionTitle = NSLocalizedString("Read", comment: "")
    static let unreadActionTitle = NSLocalizedString("Unread", comment: "")
    
    static let emptyNotificationsIconName = "empty_notifications_icon"
    static let titleForEmptyView = NSLocalizedString("No notifications", comment: "")
    static let descriptionForEmptyView = NSLocalizedString("There are no notifications for the last 30 days", comment: "")
    static let titleForEmptyViewAfterFiltering = NSLocalizedString("No notifications found", comment: "")
    static let descriptionForEmptyViewAfterFiltering = NSLocalizedString("Try another filter combination", comment: "")
    static let descriptionForEmptyViewAfterSearching = NSLocalizedString("Try another keyword", comment: "")
    
    static let markAsReadButtonTitle = NSLocalizedString("Mark as Read", comment: "")
    static let selectAllButtonTitle = NSLocalizedString("Select All", comment: "")
    static let deselectAllButtonTitle = NSLocalizedString("Deselect All", comment: "")
    static let deleteButtonTitle = NSLocalizedString("Delete", comment: "")
    static let selectedNotificationsFormat = NSLocalizedString("%d selected", comment: "")
    
    enum ConfirmationAlert {
        static let title = NSLocalizedString("Confirmation", comment: "")
        static let readSingleNotification = NSLocalizedString("Are you sure you want to mark as read 1 notification?", comment: "")
        static let readMultipleNotifications = NSLocalizedString("Are you sure you want to mark as read %d notifications?", comment: "")
        static let deleteSingleNotification = NSLocalizedString("Are you sure you want to delete 1 notification?", comment: "")
        static let deleteMultipleNotifications = NSLocalizedString("Are you sure you want to delete %d notifications?", comment: "")
        static let cancelButtonTitle = NSLocalizedString("Cancel", comment: "")
    }
}

class NotificationsViewController: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var searchBar: UISearchBar!
    private var editDoneButton: UIBarButtonItem!
    private var filtersButton: UIBarButtonItem!
    
    private let refreshControl = UIRefreshControl()
    
    private var notificationCenter: TGNotificationCenter!
    private var searchDelegate: SearchTableDelegate?  // swiftlint:disable:this weak_delegate
    private var keyboardManager: KeyboardTableSearchBarManager?
    
    var selectedNotifications: [TGNotification] {
        return tableView.indexPathsForSelectedRows?
            .map { notificationCenter.filteredNotifications[$0.row] } ?? []
    }
    
    var previouslySelectedNotifications: [TGNotification] = []
    
    var isInEditMode: Bool = false
    
    // MARK: - UI Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationCenter = AppDelegate.shared?.sharedServices.notificationCenter
        configureUI()
        loadData()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(loadData),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setupCornerRadius()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if !isInEditMode {
            tableView.setEditing(false, animated: true)
        }
    }
    
    
    private func configureUI() {
        updateTitle()
        clearBackgroundColor()
        setupBarButtons()
        tableView.backgroundColor = .clear
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        tableView.refreshControl = refreshControl
        setupCornerRadius()
        
        /*
         TODO: Empty State functionality isn't moved to Sample Project
         
         tableView.stateController.emptyViewDelegate = self
         */
        
        tableView.showsVerticalScrollIndicator = true
        tableView.allowsMultipleSelectionDuringEditing = true
        
        keyboardManager = KeyboardTableSearchBarManager(tableView: tableView)
        
        searchDelegate = SearchTableDelegate() { [weak self] searchString in
            guard let self = self else { return }
            
            self.previouslySelectedNotifications = self.selectedNotifications
            self.notificationCenter.searchQuery = searchString
            
            self.reloadView()
        }
        searchBar.delegate = searchDelegate
        searchBar.placeholder = NSLocalizedString("Search", comment: "")
        
        // Workaround to hide separator in the last cell
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
    }
    
    private func clearBackgroundColor() {
        searchBar.backgroundImage = nil
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
    }
    
    private func setupCornerRadius() {
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular {
            tableView.layer.cornerRadius = Constants.tableViewCornerRadius
            if #available(iOS 11.0, *) {
                tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            }
        } else {
            tableView.layer.cornerRadius = 0
            if #available(iOS 11.0, *) {
                tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            }
        }
    }
    
    private func setupBarButtons() {
        filtersButton = UIBarButtonItem(image: UIImage(named: "filter_icon"),
                                        style: .plain,
                                        target: self,
                                        action: #selector(showFilters))
        
        if isInEditMode {
            editDoneButton = UIBarButtonItem(barButtonSystemItem: .done,
                                             target: self,
                                             action: #selector(endEditing))
            
        } else {
            editDoneButton = UIBarButtonItem(barButtonSystemItem: .edit,
                                             target: self,
                                             action: #selector(startEditing))
        }
        
        navigationItem.rightBarButtonItems = [filtersButton, editDoneButton]
    }
    
    func setupToolbarButtons() {
        let markAsReadButton = UIBarButtonItem(title: Constants.markAsReadButtonTitle,
                                               style: .plain,
                                               target: self,
                                               action: #selector(readSelectedNotifications))
        markAsReadButton.isEnabled = !selectedNotifications.filter { !$0.isRead }.isEmpty
        
        let selectDeselectButton: UIBarButtonItem
        let allNotificationsSelected = !notificationCenter.filteredNotifications.isEmpty && notificationCenter.filteredNotifications.count == selectedNotifications.count
        if allNotificationsSelected {
            selectDeselectButton = UIBarButtonItem(title: Constants.deselectAllButtonTitle,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(deselectAllNotifications))
        } else {
            selectDeselectButton = UIBarButtonItem(title: Constants.selectAllButtonTitle,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(selectAllNotifications))
        }
        selectDeselectButton.isEnabled = !notificationCenter.filteredNotifications.isEmpty
        
        let deleteButton = UIBarButtonItem(title: Constants.deleteButtonTitle,
                                           style: .plain,
                                           target: self,
                                           action: #selector(deleteSelectedNotifications))
        deleteButton.isEnabled = !selectedNotifications.isEmpty
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolbarItems = [markAsReadButton, space, selectDeselectButton, space, deleteButton]
    }
    
    func updateTitle() {
        if isInEditMode {
            let selectedNotifcationsCount = tableView.indexPathsForSelectedRows?.count ?? 0
            title = String(format: Constants.selectedNotificationsFormat, selectedNotifcationsCount)
        } else {
            title = Constants.title
        }
    }
    
    // MARK: - Data Loading
    
    @objc
    private func loadData() {
        previouslySelectedNotifications = selectedNotifications
        
        /*
         TODO: Empty State functionality isn't moved to Sample Project
         
         tableView.stateController.isDataLoading = true
         tableView.beginRefreshingIfNeeded()
         */
        
        
        editDoneButton.isEnabled = false
        filtersButton.isEnabled = false
        
        notificationCenter.loadNotifications {  [weak self] _, error in
            guard let self = self else { return }
            
            /*
             TODO: Empty State functionality isn't moved to Sample Project
             
             self.tableView.stateController.isDataLoading = false
             */
            
            self.refreshControl.endRefreshing()
            
            if let error = error {
                self.displayError(error.localizedDescription)
            }
            
            self.didFinishLoadingData()
        }
    }
    
    @objc
    func didFinishLoadingData() {
        editDoneButton.isEnabled = !self.notificationCenter.filteredNotifications.isEmpty
        filtersButton.isEnabled = !self.notificationCenter.notifications.isEmpty
        
        reloadView()
    }
    
    private func reloadView() {
        tableView.reloadData()
        
        editDoneButton.isEnabled = !notificationCenter.filteredNotifications.isEmpty
        
        if isInEditMode {
            restorePreviouslySelectedNotifications()
            updateTitle()
            setupToolbarButtons()
        }
    }
    
    // MARK: - Actions
    
    @objc
    func showFilters() {
        // Stop scrolling before showing filters to avoid possible crash
        tableView.setContentOffset(tableView.contentOffset, animated: false)
        
        previouslySelectedNotifications = selectedNotifications
        
        let filtersViewController = NotificationsFiltersViewController(style: .grouped)
        filtersViewController.notificationCenter = notificationCenter
        filtersViewController.delegate = self
        
        let navigationController = UINavigationController(rootViewController: filtersViewController)
        navigationController.preferredContentSize = Constants.filtersModalViewContentSize
        navigationController.modalPresentationStyle = .formSheet
        
        present(navigationController, animated: true)
    }
    
    /// After performing Read / Unread action, notification will be marked as read immediately.
    /// And after that service request will be sent.
    private func markNotification(at indexPath: IndexPath, asRead: Bool) {
        let notification = notificationCenter.filteredNotifications[indexPath.row]
        let markAction = asRead ? self.notificationCenter.readNotifications : self.notificationCenter.unreadNotifications
        markAction([notification]) { [weak self] error in
            self?.handleFailingAction(with: error)
        }
        
        guard let cell = self.tableView.cellForRow(at: indexPath) as? NotificationCell else { return }
        
        cell.markAsRead(asRead) { _ in
            if !self.notificationCenter.filteredNotifications.contains(notification) {
                self.reloadView()
            }
        }
    }
    
    /// After performing Delete action, notification will be removed from data source and table view immediately.
    /// And after that service request will be sent.
    private func deleteNotification(at indexPath: IndexPath) {
        let notification = notificationCenter.filteredNotifications[indexPath.row]
        notificationCenter.deleteNotifications([notification]) { [weak self] error in
            self?.handleFailingAction(with: error)
        }
        tableView.deleteRows(at: [indexPath], with: .automatic)
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.reloadData()
        }
    }
    
    /// In case if en error occurs — it will be shown on UI and data will be reloaded.
    /// Error will be shown only of user is still on this screen.
    private func handleFailingAction(with error: NSError?) {
        if let error = error {
            displayError(error.localizedDescription)
            self.loadData()
        }
    }
    
    @objc
    func startEditing() {
        isInEditMode = true
        
        // After showing cell's action, tableView also goes into editing state
        if tableView.isEditing {
            tableView.setEditing(false, animated: true)
        }
        tableView.setEditing(true, animated: true)
        
        setupBarButtons()
        setupToolbarButtons()
        updateTitle()
        navigationController?.setToolbarHidden(false, animated: true)
    }
    
    @objc
    func endEditing() {
        isInEditMode = false
        
        tableView.setEditing(false, animated: true)
        
        setupBarButtons()
        updateTitle()
        navigationController?.setToolbarHidden(true, animated: true)
    }
    
    @objc
    func selectAllNotifications() {
        for index in notificationCenter.filteredNotifications.indices {
            let indexPath = IndexPath(row: index, section: 0)
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
        updateTitle()
        setupToolbarButtons()
    }
    
    @objc
    func deselectAllNotifications() {
        for index in notificationCenter.filteredNotifications.indices {
            let indexPath = IndexPath(row: index, section: 0)
            tableView.deselectRow(at: indexPath, animated: true)
        }
        updateTitle()
        setupToolbarButtons()
    }
    
    @objc
    func readSelectedNotifications() {
        let message: String = {
            let count = self.selectedNotifications.filter { !$0.isRead }.count
            if count == 1 {
                return Constants.ConfirmationAlert.readSingleNotification
            } else {
                return String(format: Constants.ConfirmationAlert.readMultipleNotifications, count)
            }
        }()
        
        let alert = TGAlertController(title: Constants.ConfirmationAlert.title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.ConfirmationAlert.cancelButtonTitle, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.markAsReadButtonTitle,
                                      style: .default,
                                      handler: { [weak self] _ in
                                        guard let self = self else { return }
                                        
                                        self.notificationCenter.readNotifications(self.selectedNotifications) { [weak self] error in
                                            self?.handleFailingAction(with: error)
                                        }
                                        self.endEditing()
                                        self.reloadView()
        }))
        
        present(alert, animated: true)
    }
    
    @objc
    func deleteSelectedNotifications() {
        let message: String = {
            let count = self.selectedNotifications.count
            if count == 1 {
                return Constants.ConfirmationAlert.deleteSingleNotification
            } else {
                return String(format: Constants.ConfirmationAlert.deleteMultipleNotifications, count)
            }
        }()
        
        let alert = UIAlertController(title: Constants.ConfirmationAlert.title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.ConfirmationAlert.cancelButtonTitle, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.deleteButtonTitle,
                                      style: .destructive,
                                      handler: { [weak self] _ in
                                        guard let self = self else { return }
                                        
                                        self.notificationCenter.deleteNotifications(self.selectedNotifications) { [weak self] error in
                                            self?.handleFailingAction(with: error)
                                        }
                                        self.endEditing()
                                        self.reloadView()
        }))
        
        present(alert, animated: true)
    }
}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationCenter.filteredNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotificationCell.self)) as? NotificationCell else {
            return UITableViewCell()
        }
        let notification = notificationCenter.filteredNotifications[indexPath.row]
        cell.configure(with: notification, searchTerm: notificationCenter.searchQuery)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let notificationCell = cell as? NotificationCell else { return }
        
        var cornerRadius: CGFloat?
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular {
            cornerRadius = Constants.tableViewCornerRadius
        }
        
        if indexPath.row == 0 {
            notificationCell.topCornerRadius = cornerRadius
        } else if indexPath.row == notificationCenter.notifications.count - 1 {
            notificationCell.bottomCornerRadius = cornerRadius
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let notification = notificationCenter.filteredNotifications[indexPath.row]
        
        // Read / Unread action
        let title = notification.isRead ? Constants.unreadActionTitle : Constants.readActionTitle
        let readUnreadAction = UITableViewRowAction(style: .normal, title: title) { [weak self] _, indexPath in
            self?.markNotification(at: indexPath, asRead: !notification.isRead)
            tableView.setEditing(false, animated: true)
        }
        readUnreadAction.backgroundColor = UIColor.tgOrange
        
        // Delete action
        let deleteAction = UITableViewRowAction(style: .destructive, title: Constants.deleteActionTitle) { [weak self] _, indexPath in
            self?.deleteNotification(at: indexPath)
        }
        
        return [deleteAction, readUnreadAction]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let notification = notificationCenter.filteredNotifications[indexPath.row]
        
        // Read / Unread action
        let title = notification.isRead ? Constants.unreadActionTitle : Constants.readActionTitle
        let readUnreadAction = UIContextualAction(style: .normal, title: title) { [weak self] _, _, completion in
            self?.markNotification(at: indexPath, asRead: !notification.isRead)
            completion(true)
        }
        readUnreadAction.backgroundColor = UIColor.tgOrange
        
        return UISwipeActionsConfiguration(actions: [readUnreadAction])
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        // Delete action
        let deleteAction = UIContextualAction(style: .destructive, title: Constants.deleteActionTitle) { [weak self] _, _, completion in
            self?.deleteNotification(at: indexPath)
            completion(true)
        }
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isInEditMode {
            updateTitle()
            setupToolbarButtons()
        } else {
            let notification = notificationCenter.filteredNotifications[indexPath.row]
            notificationCenter.processNotification(notification)
            markNotification(at: indexPath, asRead: true)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if isInEditMode {
            updateTitle()
            setupToolbarButtons()
        }
    }
    
    func restorePreviouslySelectedNotifications() {
        for notification in previouslySelectedNotifications {
            if let index = notificationCenter.filteredNotifications.firstIndex(where: { $0.id == notification.id }) {
                let indexPath = IndexPath(row: index, section: 0)
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
        }
    }
}

/*
 TODO: Empty State functionality isn't moved to Sample Project
extension NotificationsViewController: TGEmptyViewDelegate {
    func imageNameForEmptyView() -> String! {
        return Constants.emptyNotificationsIconName
    }
    
    func titleForEmptyView() -> String! {
        let isFilteringApplied = notificationCenter.notifications.count != notificationCenter.filteredNotifications.count
        let isSearchApplied = !(notificationCenter.searchQuery ?? "").isEmpty
        
        return isFilteringApplied || isSearchApplied ? Constants.titleForEmptyViewAfterFiltering : Constants.titleForEmptyView
    }
    
    func descriptionForEmptyView() -> String! {
        let isFilteringApplied = notificationCenter.notifications.count != notificationCenter.filteredNotifications.count
        let isSearchApplied = !(notificationCenter.searchQuery ?? "").isEmpty
        
        if isSearchApplied {
            return Constants.descriptionForEmptyViewAfterSearching
        } else {
            return isFilteringApplied ? Constants.descriptionForEmptyViewAfterFiltering : Constants.descriptionForEmptyView
        }
    }
}
*/

extension NotificationsViewController: NotificationsFiltersViewControllerDelegate {
    func didUpdateFilters() {
        reloadView()
        if tableView.numberOfRows(inSection: 0) > 0 {
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
    }
}
