//
//  KeyboardTableSearchBarManager.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

class KeyboardTableSearchBarManager {
    let tableView: UITableView
    
    init(tableView: UITableView) {
        self.tableView = tableView
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    func keyboardWillShow(_ notification: Notification) {
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        guard let window = tableView.superview?.window else {
            return
        }
        
        let keyboardRect = keyboardFrame.cgRectValue
        let convertedFrame = tableView.convert(keyboardRect, from: window)
        let keyboardSize = convertedFrame.size
        
        let contentInsets = UIEdgeInsets(top: tableView.contentInset.top,
                                         left: 0.0,
                                         bottom: keyboardSize.height,
                                         right: 0.0)
        
        tableView.contentInset = contentInsets
        tableView.scrollIndicatorInsets = contentInsets
    }
    
    @objc
    func keyboardWillHide(_ notification: Notification) {
        tableView.contentInset = .zero
        tableView.scrollIndicatorInsets = .zero
    }
}
