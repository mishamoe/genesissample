//
//  SelectItemCell.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/16/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

class SelectItemCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkmarkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        checkmarkImageView.tintColor = .tgOrange
        titleLabel.textColor = .tgCellText
        titleLabel.font = .tgSubtitle
        titleLabel.numberOfLines = 2
        selectionStyle = .none
    }
    
    func configureForInfoPresentation() {
        titleLabel.numberOfLines = 0
    }
}
