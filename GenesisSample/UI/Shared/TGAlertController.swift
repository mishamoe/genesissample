//
//  TGAlertController.swift
//  GenesisSample
//
//  Created by Mykhailo Moiseienko on 8/17/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import UIKit

class TGAlertController: UIAlertController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.tintColor = .tgOrange
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.tintColor = .tgOrange
    }
    
    func updateMessage(_ message: String) {
        self.message = message
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
}
