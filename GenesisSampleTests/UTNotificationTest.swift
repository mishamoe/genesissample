//
//  UTNotificationTest.swift
//  GenesisSampleTests
//
//  Created by Mykhailo Moiseienko on 8/14/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import XCTest
@testable import GenesisSample

class UTNotificationTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMakeNotification() throws {
        let notification = try TGNotification.makeNotification()
        
        XCTAssertNotNil(notification.createdAt)
        XCTAssertEqual(notification.id, 3838)
        XCTAssertTrue(notification.isRead)
        XCTAssertEqual(notification.typeId, 3)
        XCTAssertEqual(notification.type, TGNotificationType.inventory)
    }


}

private extension TGNotification {
    static func makeNotification() throws -> TGNotification {
        let json: [String: Any] = [
            "apn": [
                "apnId": "ba84cfea-89a1-484d-819c-10d0fb1942b3",
                "aps": [
                    "alert": [
                        "action-loc-key": "View",
                        "body": "Transfer Request 14018 was created",
                        "title": "Transfer Request Created"
                    ],
                    "content-available": 0
                ],
                "custom-actions": [
                    "action": 20,
                    "entityId": 8,
                    "entityName": 14018,
                    "entityType": 8,
                    "target": "null"
                ]
            ],
            "createdAt": "2019-08-14T13:28:53",
            "id": 3838,
            "isRead": true,
            "type": 3
        ]
        
        let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.utc)
        return try decoder.decode(TGNotification.self, from: data)
    }
}
