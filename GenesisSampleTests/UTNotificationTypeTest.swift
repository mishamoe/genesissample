//
//  UTNotificationTypeTest.swift
//  GenesisSampleTests
//
//  Created by Mykhailo Moiseienko on 8/14/19.
//  Copyright © 2019 Mykhailo Moiseienko. All rights reserved.
//

import XCTest
@testable import GenesisSample

class UTNotificationTypeTest: XCTestCase {
    func testOther() {
        let otherType = TGNotificationType(rawValue: 0)
        
        XCTAssertNotNil(otherType)
        XCTAssertEqual(otherType, TGNotificationType.other)
    }
    
    func testRescheduleRequests() {
        let rescheduleRequestsType = TGNotificationType(rawValue: 1)
        
        XCTAssertNotNil(rescheduleRequestsType)
        XCTAssertEqual(rescheduleRequestsType, TGNotificationType.rescheduleRequests)
    }
    
    func testIMSDispatches() {
        let imsDispatchesType = TGNotificationType(rawValue: 2)
        
        XCTAssertNotNil(imsDispatchesType)
        XCTAssertEqual(imsDispatchesType, TGNotificationType.imsDispatches)
    }
    
    func testInventory() {
        let inventoryType = TGNotificationType(rawValue: 3)
        
        XCTAssertNotNil(inventoryType)
        XCTAssertEqual(inventoryType, TGNotificationType.inventory)
    }

    func testJobAssignment() {
        let jobAssignmentType = TGNotificationType(rawValue: 5)
        
        XCTAssertNotNil(jobAssignmentType)
        XCTAssertEqual(jobAssignmentType, TGNotificationType.jobAssignment)
    }
    
    func testUnknownType() {
        let unknownType = TGNotificationType(rawValue: 999)
        
        XCTAssertNil(unknownType)
    }
}
