# Sample Project for Genesis
---
## Project Description
The given project represents a module from the production application TechGenie.
This module provides the functionality named **"Notifications"**.

It allows users to:

* view all of the notifications that were sent by the system

* read the information provided in the notifications

* interact with the notifications to trigger the associated actions

* mark the notifications as read / unread

* delete the notifications individually or in bulk quantities

The **Business Context** that led to the implementation of this module is described below.

#### Business Context
Communication to the field is essential to keeping Smart Home Professionals up to date on recent events. Smart Home Professionals currently are unable to go back to notifications after they have been dismissed, resulting in some communications being missed.

When Tech Genie users dismiss a received notification, there is no way to retroactively open the notification, as it is not archived anywhere. This causes scenarios where the user is not able to address the notification at that moment, dismisses it, and has no way of going back to it later.

## Project Architecture
#### Layered Application Structure
Mobile application is based on Model-View-Controller (MVC) pattern and consists from several layers

* **Data Access layer** is responsible for providing access to application's data. The data may be stored in a local cache or retrieved from backend services.

* **Business layer** is responsible for app logic and behaviors. It is deals with things like data processing, transformation, and management as well as business rules.

* **Presentation layer** serves as communication point between end user an app logic.

* **Persistence layer** is responsible for manipulations with local database and caching data downloaded from web-services.

![Architecture](./Architecture.png)
